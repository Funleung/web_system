const mongoose   = require('mongoose');

const DB_HOST = [
  'cluster0-shard-00-00-65rnq.mongodb.net:27017',
  'cluster0-shard-00-01-65rnq.mongodb.net:27017',
  'cluster0-shard-00-02-65rnq.mongodb.net:27017'
];
const DB_NAME = 'crashLog';

const DB_RS_NAME = 'Cluster0-shard-0';
const DB_USE_SSL = true;
const DB_USER = 'cnq0512';
const DB_PASS = 'cnq0512';
const DB_AUTHDB = 'admin';

module.exports = {
    connect: () => new Promise(resolve => {
        mongoose.connect(`mongodb://${DB_HOST.join()}/${DB_NAME}`, {
            replset:{
                rs_name: DB_RS_NAME,
                ssl: DB_USE_SSL
            },
            user: DB_USER,
            pass: DB_PASS,
            auth: {
                authdb: DB_AUTHDB
            }
        });

        const db = mongoose.connection;

        db.on('open', () => {
            console.log(`Connected to ${db.name}`);
            resolve();
        });

        db.on('error', (err) => {
            console.error('Connection error:', err);
            console.log('Process terminated');
            process.exit(1);
        });

        process.on('SIGINT', () => {
            db.close(() => {
                console.log('Mongoose disconnected due to app termination');
                process.exit(0);
            });
        });
    })
} 
