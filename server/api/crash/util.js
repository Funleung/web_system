class Util{
  constructor(){
    this.replace_dot = new RegExp(/\./, 'g');
    // this.crashInfo = new RegExp(/(.+: .+)\\n\\t.+\(([\w.:\d]+)\)/, 'u'); //Find exception, message, source
  }

  convertFields(src){
    var target = {}
    for (let key of Object.keys(src)) {
      let field = key.replace(this.replace_dot, '-');
      target[field] = src[key];
      if(typeof(src[key]) === 'object'){
        target[field] = this.convertFields(target[field]);
      }
    }
    return target;
  }

  getPosition(string, subString, index) {
    var position = string.split(subString, index).join(subString).length;
    return position;
  }

  findWords(src){
    // console.log('> Stack Trace Info (Exception, Message, Source)')

    var n = src.indexOf(': ');

    var exception = src.slice(0, n); // Exception
    var msg = src.slice(n + 2 , src.indexOf('\n')) // Message

    var last = this.getPosition(src,'\n',2)-1;
    var target = src.substring(0,last+1);


    // var source = src.slice(src.indexOf('(') + 1 , src.indexOf(')')) // Source

    var source = target.slice(target.lastIndexOf('(') + 1 , last)

    // console.log(exception)
    // console.log(msg);
    // console.log(source)

    this.exceptionInfo = {
      EXCEPTION: exception,
      MESSAGE: msg,
      SOURCE: source
    }
    return this.exceptionInfo;
  }

  insertIssue(src) {
    // var crashId = [];
    var result = {};
    var info = this.exceptionInfo;
    var appVersion = src['APP_VERSION_NAME'];
    var build = src.BUILD_CONFIG.BUILD_TYPE;
    var seen = src['USER_CRASH_DATE'];
    var stackTrace = src['STACK_TRACE'];
    var instant = 1;

    this.issueInfo = {
      EXCEPTION: info['EXCEPTION'],
      MESSAGE: info['MESSAGE'],
      SOURCE: info['SOURCE'],
      APP_VERSION_NAME: appVersion,
      BUILD_TYPE: build,
      FIRST_SEEN: seen,
      LAST_SEEN: seen,
      LAST_STACK_TRACE: stackTrace,
      INSTANT: instant
    }
    return this.issueInfo;
  }

  dateCompare(t1, t2) {
    var n1 = t1;
    var n2 = t2;
    var rs = n2;
    //
    // console.log(t1)
    // console.log(t2)

    n1 = t1.slice(0, t1.indexOf('.'))
    n2 = t2.slice(0, t1.indexOf('.'))

    if (n1 > n2) {
      rs = n1;
    }
    return rs;
  }


}
module.exports = new Util();
