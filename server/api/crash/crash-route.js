const crashDetailService = require('./crash-service');
const AppService = require('../app/app-service');
const IssueService = require('../issue/issue-service');
const util = require("./util");

module.exports = app => {
  // Insert new crash detail
  app.post('/:apikey', (req, res, next) => {
    req.body.API_KEY = req.params.apikey;

    if(!req.body.STACK_TRACE || !req.body.BUILD_CONFIG){
      res.sendStatus(500);
      return;
    } else if(!req.body.BUILD_CONFIG.BUILD_TYPE){
      res.sendStatus(500);
      return;
    }

    App.findOne({ API_KEY: req.params.apikey }, (err, app) => {
      /*if(err){
        res.sendStatus(403);
        return;
      }*/

      if(!app){ //no such apikey
        res.sendStatus(500);
        return;
      }

      //console.log('> Main: Log process');

      var log = util.convertFields(req.body);
      var exceptionInfo = util.findWords(log['STACK_TRACE']); // Change '.' into '-'

      log = Object.assign({}, log, exceptionInfo);

      // console.log('util ok.')
      // console.log(log)

      //console.log(`> Main: Parse data to ${DB_NAME}`);
      var app = null;
      var crashId = null;
      var issueId = null;


      // Insert a crash log
      crashDetailService.create(log)
        .then((log) => {   // issue task
          return crashDetailService.findOneById(log._id);
        })
        .then((log) => {
          crashId = log._id;
          return AppService.findByApiKey({'API_KEY': log['API_KEY']})
        })
        .then((a) => { // 'a' refers to the corresponding app
          app = a;
          appId = app._id;
          // console.log(appId)

          // console.log('Start issueIsExist');
          var query = {
            // 'API_KEY':log['API_KEY'],
            'APP_ID': appId,
            'EXCEPTION':log['EXCEPTION'],
            'MESSAGE': log['MESSAGE'],
            'SOURCE': log['SOURCE'],
            'APP_VERSION_NAME': log['APP_VERSION_NAME'],
            'BUILD_TYPE': log.BUILD_CONFIG.BUILD_TYPE
          }
          //console.log("-------query");
          //console.log(query);
          return IssueService.getIssueItem(query)
        })
        .then((issue)=>{
          /* Create a new issue */

          // console.log(issue)

          if (issue == '' || issue == null) {
            var result = util.insertIssue(log);
            // Not exists, create new

            appId = app._id;

            // if(!result["CRASHES"]){
            //   result["CRASHES"] = [];
            // }
            // result["CRASHES"].push(crashId);
            // issue["APP_ID"] = appId;
            issue = Object.assign({'APP_ID': appId}, result, {'INSTANCE': 1});
            //console.log(issue)
            return IssueService.create(issue);

          } else {
            /* Update an existing issue */

            issueId = issue._id;

            // issue['LAST_SEEN'] = util.dateCompare(log['USER_CRASH_DATE'], issue['LAST_SEEN'])
            // issue['LAST_STACK_TRACE'] = log['STACK_TRACE'];
            // var count = issue['INSTANT'] + 1;
            // console.log('------------')
            // console.log(count)
            // console.log(typeof count)
            // issue['INSTANT'] = issue['INSTANT'] + 1;
            //
            // console.log(issue['INSTANT'])
            //issue = Object.assign();

            // TODO: updateCrashReference
            // return IssueService.updateCrashReference(issueId, crashId);
            return IssueService.updateCrashInstant(issueId, log['USER_CRASH_DATE'], log['STACK_TRACE']);
          }
        })
        .then((raw) => {
          // console.log("----")
          // console.log(raw);

          // console.log(issueId)
          // var issueId = null;
          if(raw == '' || raw == null) {
            // continue;
          }
          else if(raw._id) {
            issueId = raw._id;
          }
          // var issueId = raw.issueId;
          // console.log('**********')
          // console.log(result)
          // console.log()
          // console.log(`Issue ID: ${issueId}`)
          // console.log(`Crash ID: ${crashId}`)
          return crashDetailService.updateCrash(issueId, crashId);
        })
        // .then((rs) => {
        //   console.log('--------------------')
        //   console.log(`Issue ID: ${rs}`)
        //   console.log(`Crash ID: ${crashId}`)
        // })
        .then(() => {
          //console.log('> Main: OK ! Data created.')
          //res.send("OK ! Data created.")
          // console.log('then')
          res.sendStatus(200);
        })
        .catch((err) => {
          //console.log(err);
          // console.log(err);
          // res.sendStatus(500);
          res.sendStatus(err);
        });
    });
  });

  app.get('/api/crashes/:crashId', (req, res) => {
    if(!req.user){
      res.sendStatus(403);
      return;
    }

    let crashId = req.params.crashId;
    crashDetailService.getCrash(crashId)
      .then(crashDetails =>{
        res.json(crashDetails);
      })
      .catch(err => {
        res.sendStatus(500);
      });
  });

  app.get('/api/crashes', (req, res) => {
    if(!req.user){
      res.sendStatus(403);
      return;
    }

    crashDetailService.getCrashes(req.query)
      .then(crashes => {
        res.json(crashes);
      })
      .catch((err) => {
        res.sendStatus(500);
      });
  });
}
