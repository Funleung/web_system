// DB
Issue = require('../issue/issue-schema');
CrashDetails = require('./crash-schema');

const LIMIT = 16;
// Mongoose
let mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId

const getCrashes = params => {
  //params => {issueId, page, os, mac, keyword, opt}
  return new Promise((resolve, reject) => {
    if(!ObjectId.isValid(params.issueId)){
      //return empty result
      resolve({
        crashes: [],
        count: 0
      });
    }

    let query = {ISSUE_ID: ObjectId(params.issueId)};

    if(params.os){
      query.ANDROID_VERSION = params.os;
    }

    if(params.mac){
      const macRegex = /([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2})|([0-9a-fA-F]{2}):?([0-9a-fA-F]{2})/i;
      const macSplit = params.mac.match(macRegex);

      if(macSplit){ //mac valid
        let macMatchRegex;
        if(params.mac.length > 5){
          macMatchRegex = new RegExp(`${macSplit[1]}:${macSplit[2]}:${macSplit[3]}:${macSplit[4]}:${macSplit[5]}:${macSplit[6]}`, 'i');
        } else {
          macMatchRegex = new RegExp(`[0-9a-fA-F:]+${macSplit[7]}:?${macSplit[8]}`, 'i');
        }
        query['CUSTOM_DATA.mac'] = macMatchRegex;
      }
    }

    if(params.keyword){
      const regex = {$regex: new RegExp(`.*${params.keyword}.*`, 'i')};

      switch(params.opt){
        default:
        case 'Stack Trace':
          query.STACK_TRACE = regex;
          break;
        case 'Logcat':
          query.LOGCAT = regex;
          break;
      }
    }

    let sortOrder = {};

    switch(params.orderBy) {
      default:
      case 'Latest Issue':
        sortOrder.USER_CRASH_DATE = -1;
        break;
      case 'Oldest Issue':
        sortOrder.USER_CRASH_DATE = 1;
        break;
      case 'OS version':
        sortOrder.ANDROID_VERSION = -1;
        break;
    }
    
    CrashDetails.aggregate()
      .match(query)
      .facet({
        crashes: [
          {
            $sort: sortOrder
          },
          {
            $skip: LIMIT * ((params.page ? params.page : 1) - 1)
          },
          {
            $limit: LIMIT
          },
          {
            $project: {
              crashDate: '$USER_CRASH_DATE',
              mac: '$CUSTOM_DATA.mac',
              osVersion: '$ANDROID_VERSION'
            }
          }
        ],
        count: [{$count: 'count'}]
      })
      .then(res => {
        if(res[0].crashes.length > 0){
            resolve({
              crashes: res[0].crashes,
              count: res[0].count[0].count
            });
          } else {
            resolve({
              crashes: [],
              count: 0
            })
          }
      });
  });
}

const getCrash = crashId => CrashDetails.findById(crashId);

const updateCrash = (issueId, crashId) => {
  return CrashDetails.update(
    { "_id": crashId },
    { $set: { "ISSUE_ID": issueId } }
  );
}


let findOneById = (id = {}) => {
  // Promise
  return CrashDetails.findById({_id: id})
}

let findIssueAppId = () => {
  return CrashDetails.findOne();
}

let create = (body = {}) => {
  // console.log("> Service: Create Crashes Log")
  // console.log(body)
  // Promise
  return CrashDetails.create(body)
}

let update = (query = {}, update = {}, upsert = {}) => {
  //Promise
  return CrashDetails.update(query, update, upsert = true)
}

let removeById = (id = {}) => {
  //Promise
  return CrashDetails.deleteOne({_id: id})
}

let remove = (query = {}, project = {}, opts = {}) => {
  //Promise
  return CrashDetails.deleteOne(query, project, opts)
}

module.exports = {
  findOneById,
  findIssueAppId,
  create,
  update,
  removeById,
  remove,
  getCrash,
  getCrashes,
  updateCrash
}
