// DB
Users = require('./user-schema')

// Mongoose
let mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId

let find = (query = {}, project = {}, opts = {}) => {
  // Promise
  return Users.find(query, project = {password:0}, opts)
}

let findOne = (query = {}, project = {}, opts = {}) => {
  // Promise
  return Users.findOne(query, project = {password:0}, opts)
}

let findOneById = (id = {}) => {
  // Promise
  return Users.findOne({_id: id})
}

// Operation (Insert / Update / Delete)

let create = (body = {}) => {
  // Promise
  return Users.create(body)
}

let update = (query = {}, update = {}, upsert) => {
  //Promise
  return Users.update(query, update, upsert = true)
}

let removeById = (id = {}) => {
  //Promise
  return Users.deleteOne({_id: id})
}

let remove = (query = {}, project = {}, opts = {}) => {
  //Promise
  return Users.deleteOne(query, project, opts)
}

module.exports = {
  find,
  findOne,
  findOneById,
  create,
  update,
  removeById,
  remove
}
