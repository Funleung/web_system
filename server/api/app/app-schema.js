const mongoose = require('mongoose');

const AppSchema = mongoose.Schema({
    API_KEY: String,
    PLATFORM: String,
    APP_NAME: String,
});

const App = mongoose.model('app', AppSchema);

module.exports = App;
