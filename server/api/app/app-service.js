// DB
App = require('./app-schema');
Issue = require('../issue/issue-schema');

// Mongoose
let mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId

const getAllApps = () => {
  return App.aggregate()
    .project({
      apikey: '$API_KEY',
      platform: '$PLATFORM',
      name: '$APP_NAME'
    })
};

const getAppById = appId => {
  return new Promise((resolve) => {
    App.findById(appId)
      .then(res => {
        resolve({
          _id: res._id,
          //apikey: res.API_KEY,
          platform: res.PLATFORM,
          name: res.APP_NAME
        })
      })
  });
}

const getAppByMac = (appId, mac) => {
  return new Promise((resolve, reject) => {
    const macRegex = /([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2})|([0-9a-fA-F]{2}):?([0-9a-fA-F]{2})/i;
    const macSplit = mac.match(macRegex);

    if(!macSplit){
      //mac not valid
      reject();
    }

    let macMatchRegex;
    if(mac.length > 5){
      macMatchRegex = new RegExp(`${macSplit[1]}:${macSplit[2]}:${macSplit[3]}:${macSplit[4]}:${macSplit[5]}:${macSplit[6]}`, 'i');
    } else {
      macMatchRegex = new RegExp(`[0-9a-fA-F:]+${macSplit[7]}:?${macSplit[8]}`, 'i');
    }

    App.aggregate()
      .match({_id: ObjectId(appId)})
      .facet({
        app: [{
          $project: {
            platform: '$PLATFORM',
            name: '$APP_NAME'
          }
        }],
        mac: [
          {
            $lookup: {
              from: "issues",
              localField: "_id",
              foreignField: "APP_ID",
              as: "issue"
            }
          },
          {
            $unwind: '$issue'
          },
          {
            $replaceRoot: {newRoot: '$issue'}
          },
          {
            $lookup: {
              from: "crashes",
              localField: "_id",
              foreignField: "ISSUE_ID",
              as: "crash"
            }
          },
          {
            $unwind: '$crash'
          },
          {
            $match: {'crash.CUSTOM_DATA.mac': {$regex: macMatchRegex}}
          },
          {
            $group: {
              _id: null,
              mac: {$addToSet: '$crash.CUSTOM_DATA.mac'}
            }
          }
        ]
      })
      .then(res => {
        let json = {
          _id: res[0].app[0]._id,
          platform: res[0].app[0].platform,
          name: res[0].app[0].name
        };

        if(res[0].mac.length){
          json.macs = res[0].mac[0].mac;
        } else {
          json.macs = [];
        }

        resolve(json);
      });
  });
};

let create = (body = {}) => {

  // Promise
  return App.create(body)
}

let findByApiKey = (apiKey) => {

  return App.findOne(apiKey);
}
/*let find = (query = {}, project = {}, opts = {}) => {
  // Promise
  return Apps.find(query, project, opts)
}

let findOne = (query = {}, project = {}, opts = {}) => {
  // Promise
  return Apps.findOne(query, project, opts)
}

let findOneById = (id = {}) => {
  // Promise
  return Apps.findOne({_id: id})
}

// Operation (Insert / Update / Delete)


let update = (query = {}, update = {}, upsert = {}) => {
  //Promise
  return Apps.update(query = {API_KEY: apiKey}, update, upsert = true)
}

let removeById = (id = {}) => {
  //Promise
  return Apps.deleteOne({_id: id})
}

let remove = (query = {}, project = {}, opts = {}) => {
  //Promise
  return Apps.deleteOne(query, project, opts)
}*/

module.exports = {
  getAllApps,
  getAppById,
  getAppByMac,
  //find,
  //findOne,
  //findOneById,
  findByApiKey,
  create,
  //update,
  //removeById,
  //remove
}
