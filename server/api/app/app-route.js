const appService = require('./app-service')

module.exports = (app) => {
  // Get All apps
  app.get('/api/apps', (req, res) => {
    if(!req.user){
      res.sendStatus(403);
      return;
    }

    appService.getAllApps()
      .then((apps) => {
        res.json(apps)
      })
      .catch((err) => {
        res.sendStatus(500);
      })
  });

  app.get('/api/apps/:appId', (req, res) => {
    if(!req.user){
      res.sendStatus(403);
      return;
    }

    let promise;

    if(req.query.mac){
      promise = appService.getAppByMac(req.params.appId, req.query.mac);
    } else {
      promise = appService.getAppById(req.params.appId);
    }
    
    promise
      .then(app => res.json(app))
      .catch(err => console.log(err));
  });

  app.post('/api/apps', (req, res) => {
    if(!req.user){
      res.sendStatus(403);
      return;
    }

    let body = req.body;
    let apiKey = req.body.API_KEY;
    let appName = req.body.APP_NAME;
    let platform = req.body.PLATFORM;

    appService.create(body)
      .then(() => {
        //res.send('New app created ! ' + `API key: ${apiKey}, Applicaiton name: ${appName}, Platform: ${platform}`)
        res.sendStatus(200);
      })
      .catch((err) => {
        res.sendStatus(500);
      })
  });

}
