const IssueService = require('./issue-service');
const CrashService = require('../crash/crash-service');

module.exports = (app) => {

  app.get('/api/issues', (req, res) => {
    //req.query => {issueIds, page, orderBy, os, app, keyword, opt}

    if(!req.user){
      res.sendStatus(403);
      return;
    }

    IssueService.getIssues(req.query)
      .then(issues => res.json(issues))
      .catch(err => {
        console.log(err)
        res.sendStatus(500)
      });
  });

  app.get('/api/issues/:issueId', (req, res) => {
    if(!req.user){
      res.sendStatus(403);
      return;
    }

    IssueService.getIssue(req.params.issueId)
      .then(issue => res.json(issue))
      .catch(err => res.sendStatus(500));
  });

}
