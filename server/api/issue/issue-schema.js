let mongoose = require('mongoose');


mongoose.Promise = global.Promise

const IssueSchema = mongoose.Schema({
  APP_ID: Object,
  BUILD_TYPE: String,
  EXCEPTION: String,
  MESSAGE: String,
  SOURCE: String,
  APP_VERSION_NAME: String,
  FIRST_SEEN: String,
  LAST_SEEN: String,
  LAST_STACK_TRACE: String,
  INSTANCE: Number
  // APP_ID: Object
});

module.exports = mongoose.model('issue', IssueSchema);
