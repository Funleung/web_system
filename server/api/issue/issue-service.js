// DB
Issues = require('./issue-schema');

const LIMIT = 5;

// Mongoose
const mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId

const getIssues = params => {
  //params => {appId, build, page, orderBy, app, os, keyword, opt}

  return new Promise((resolve, reject) => {
    if(!ObjectId.isValid(params.appId)){
      //return empty result
      resolve({
        issues: [],
        count: 0
      });
    }

    let query;
    let sortOrder = {};

    query = { APP_ID: ObjectId(params.appId), 'BUILD_TYPE': params.build ? params.build : 'debug' };

    if(params.app){
      query.APP_VERSION_NAME = params.app;
    }

    switch(params.orderBy) {
      default:
      case 'Latest Issue':
        sortOrder.FIRST_SEEN = -1;
        break;
      case 'Oldest Issue':
        sortOrder.FIRST_SEEN = 1;
        break;
      case 'Recent Crashed':
        sortOrder.LAST_SEEN = -1;
        break;
      case 'Crash instances':
        sortOrder.INSTANCE = -1 ;
        break;
      case 'App version':
        sortOrder.APP_VERSION_NAME = 1;
        break;
    }

    if(params.mac || params.os || params.keyword){
      let having = {};

      if(params.mac){
        const macRegex = /([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2}):?([0-9a-fA-F]{2})|([0-9a-fA-F]{2}):?([0-9a-fA-F]{2})/i;
        const macSplit = params.mac.match(macRegex);

        if(macSplit){ //mac valid
          let macMatchRegex;
          if(params.mac.length > 5){
            macMatchRegex = new RegExp(`${macSplit[1]}:${macSplit[2]}:${macSplit[3]}:${macSplit[4]}:${macSplit[5]}:${macSplit[6]}`, 'i');
          } else {
            macMatchRegex = new RegExp(`[0-9a-fA-F:]+${macSplit[7]}:?${macSplit[8]}`, 'i');
          }
          having.MAC = macMatchRegex;
        }
      }

      if(params.os){
        having.ANDROID_VERSION = params.os;
      }

      if(params.keyword){
        const regex = {$regex: new RegExp(`.*${params.keyword}.*`, 'i')};

        switch(params.opt){
          default:
          case 'Stack Trace':
            having.STACK_TRACE = regex;
            break;
          case 'Logcat':
            having.LOGCAT = regex;
            break;
        }
      }

      Issues.aggregate()
        .match(query)
        .lookup({
          from: 'crashes',
          localField: '_id',
          foreignField: 'ISSUE_ID',
          as: 'crashes'
        })
        .unwind('$crashes')
        .group({
          _id: '$_id',
          BUILD_TYPE: {$first: '$BUILD_TYPE'},
          EXCEPTION: {$first: '$EXCEPTION'},
          MESSAGE: {$first: '$MESSAGE'},
          SOURCE: {$first: '$SOURCE'},
          INSTANCE: {$first: '$INSTANCE'},
          APP_VERSION_NAME: {$first: '$APP_VERSION_NAME'},
          FIRST_SEEN: {$first: '$FIRST_SEEN'},
          LAST_SEEN: {$first: '$LAST_SEEN'},
          MAC: {$addToSet: '$crashes.CUSTOM_DATA.mac'},
          ANDROID_VERSION: {$addToSet: '$crashes.ANDROID_VERSION'},
          STACK_TRACE: {$push: '$crashes.STACK_TRACE'},
          LOGCAT: {$push: '$crashes.LOGCAT'},
        })
        .match(having)
        .facet({
          issues: [
            {
              $sort: sortOrder
            },
            {
              $skip : (LIMIT * ((params.page ? params.page : 1) - 1))
            },
            {
              $limit: LIMIT
            },
            {
              $project:{
                build: '$BUILD_TYPE',
                exception: '$EXCEPTION',
                message: '$MESSAGE',
                source: '$SOURCE',
                instance: '$INSTANCE',
                appVersion: '$APP_VERSION_NAME',
                firstSeen: '$FIRST_SEEN',
                lastSeen: '$LAST_SEEN',
                mac: '$MAC'
              }
            }
          ],
          count: [{$count: 'count'}]
        })
        .then(res => {
          if(res[0].issues.length > 0) {
            resolve({
              issues: res[0].issues,
              count: res[0].count[0].count
            });
          } else {
            resolve({
              issues: [],
              count: 0
            })
          }
        })
    } else {  // no need to lookup
      Issues.aggregate()
        .match(query)
        .facet({
          issues: [
            {
              $sort: sortOrder
            },
            {
              $skip : (LIMIT * ((params.page ? params.page : 1) - 1))
            },
            {
              $limit: LIMIT
            },
            {
              $project: {
                build: '$BUILD_TYPE',
                exception: '$EXCEPTION',
                message: '$MESSAGE',
                source: '$SOURCE',
                instance: '$INSTANCE',
                appVersion: '$APP_VERSION_NAME',
                firstSeen: '$FIRST_SEEN',
                lastSeen: '$LAST_SEEN'
              }
            }
          ],
          count: [{$count: 'count'}]
        })
        .then(res => {
          if(res[0].issues.length > 0){
            resolve({
              issues: res[0].issues,
              count: res[0].count[0].count
            });
          } else {
            resolve({
              issues: [],
              count: 0
            })
          }
        });
    }
  })


    /*cursor.then(corsor => {
      console.log(cursor);
    });*/

    /*et count = 0;
    let issues = [];
    cursor.eachAsync(doc => {
      issues.push(doc);
      count++;
    });

    return {
      data: issues,
      count: count
    }*/
      /*.facet({
        data: [
          {
            $sort: sortOrder
          },
          {
            $skip: LIMIT * ((params.page ? params.page : 1) - 1)
          },
          {
            $limit: LIMIT
          },
          {
            $project: {
              build: '$BUILD_TYPE',
              exception: '$EXCEPTION',
              message: '$MESSAGE',
              source: '$SOURCE',
              instance: '$INSTANCE',
              appVersion: '$APP_VERSION_NAME',
              firstSeen: '$FIRST_SEEN',
              lastSeen: '$LAST_SEEN'
            }
          }
        ],
        count: [{
          $count: 'count'
        }]
      })
      .project({
        data: '$data',
        count: {$arrayElemAt: [ '$count.count', 0 ]}
      });*/
};

  /*if(params.app){
    query.APP_VERSION_NAME = params.app;
  }

  if(params.os){
    having.osVersion = params.os;
  }

  if(params.keyword){
    const regex = {$regex: new RegExp(`.*${params.keyword}.*`, 'i')};

    switch(params.opt){
      default:
      case 'Stack Trace':
        having.stackTrace = regex;
        break;
      case 'Logcat':
        having.logcat = regex;
        break;
    }
  }

  switch(params.orderBy) {
    default:
    case 'Latest Issue':
      sortOrder.firstSeen = -1;
      break;
    case 'Oldest Issue':
      sortOrder.firstSeen = 1;
      break;
    case 'Recent Crashed':
      sortOrder.lastSeen = -1;
      break;
    case 'Crash instances':
      sortOrder.instance = -1 ;
      break;
    case 'App version':
      sortOrder.appVersion = 1;
      break;
  }

  return Issues.aggregate()
    .match(query)
    .lookup({
      from: 'crashes',
      localField: 'CRASHES',
      foreignField: '_id',
      as: 'crasheDetails'
    })
    .unwind('$crasheDetails')
    .group({
      _id: '$_id',
      build: {$first: '$BUILD_TYPE'},
      exception: {$first: '$EXCEPTION'},
      message: {$first: '$MESSAGE'},
      source: {$first: '$SOURCE'},
      instance: {$sum: 1},
      appVersion: {$first: '$APP_VERSION_NAME'},
      osVersion: {$addToSet: '$crasheDetails.ANDROID_VERSION'},
      stackTrace: {$push: '$crasheDetails.STACK_TRACE'},
      // stackTrace: {$push: '$LAST_STACK_TRACE'},
      logcat: {$push: '$crasheDetails.LOGCAT'},
      firstSeen: {$first: '$crasheDetails.USER_CRASH_DATE'},
      // firstSeen: {$first: '$FIRST_SEEN'},
      lastSeen: {$last: '$crasheDetails.USER_CRASH_DATE'},
      // lastSeen: {$last: '$LAST_SEEN'},
    })
    .match(having)
    .project({
      build: '$build',
      exception: '$exception',
      message: '$message',
      source: '$source',
      instance: '$instance',
      appVersion: '$appVersion',
      firstSeen: '$firstSeen',
      lastSeen: '$lastSeen'
    })
    .facet({
      'issues': [
        {$sort: sortOrder},
        {$skip: LIMIT * ((params.page ? params.page : 1) - 1)},
        {$limit: LIMIT},
      ],
      'count': [{$count: 'count'}]
    })
    .project({
      issues: '$issues',
      count: {$arrayElemAt: [ '$count', 0 ]}
    })
    .project({
      issues: '$issues',
      count: '$count.count'
    });*/

const getIssue = issueId => {
  return new Promise((resolve, reject) => {
    Issues.aggregate()
      .match({_id: ObjectId(issueId)})
      .project({
        appId: '$APP_ID',
        build: '$BUILD_TYPE',
        exception: '$EXCEPTION',
        message: '$MESSAGE',
        source: '$SOURCE',
        appVersion: '$APP_VERSION_NAME',
        instance: '$INSTANCE',
        firstSeen: '$FIRST_SEEN',
        lastSeen: '$LAST_SEEN',
        latestCrashStackTrace: '$LAST_STACK_TRACE'
      })
      .then(res => {
        resolve(res[0]);
      });
  });
}

const getIssueItem = (query = {}, project = {}, opts = {}) => {
  return Issues.findOne(query, project, opts)
}

const findAppIdByApiKey = (query = {}, project = {}, opts = {}) => {
  return Issues.findOne(query, project, opts)
}

let create = (body = {}) => {
  return Issues.create(body)
}

let update = (query = {}, update = {}, upsert = true) => {
  return Issues.findOneAndUpdate(query, update, upsert)
}

let updateCrashReference = (issueId, crashId) => {
  // console.log(`IssueID: ${issueId}`);
  // console.log(`CrashID: ${crashId}`);
  // console.log(`Type of crashId: ${typeof crashId}`)
  // Issues.find({_id: issueId}).then((res)=>{
  //   console.log(res);
  // });
  return Issues.update(
    { _id: issueId },
    {
      $push: { 'CRASHES': crashId }
    }
  )
}

let updateCrashInstant = (issueId, lastSeen, stackTrace) => {
  return Issues.update(
    {_id: issueId },
    {
      $set: { 'LAST_SEEN': lastSeen, 'LAST_STACK_TRACE': stackTrace},
      $inc: { 'INSTANCE':  1}
    }
  )
}

// let findAndRemoveLastIssue = () => {
//   var target = Issues.remove({},{_id}).sort({'_id':-1}).limit(1);
//   return ;
// }

module.exports = {
  //getAllIssues, //debug
  getIssue,
  getIssues,
  findAppIdByApiKey,
  //getLatestCrashStackTrace,

  getIssueItem,
  create,
  update,
  updateCrashReference,
  updateCrashInstant
  // findAndRemoveLastIssue
  /*getIssuesByOSVersion,
  getIssuesByAppVersion,
  getIssuesByKeyword*/
}
