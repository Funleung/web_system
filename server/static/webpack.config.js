const webpack = require('webpack');

module.exports = {
  context: `${__dirname}`,
  entry: {
    login: './login/js/main.js',
    main: './main/js/main.js'
  },
  output: {
    path: `${__dirname}/dist`,
    filename: '[name]-bundle.js'
  },
  module: {
    loaders: [
        { test: /\.css$/, 
          use: [
            { loader: "style-loader" },
            { loader: "css-loader" }
          ] 
        },
        {
          test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
          loader: 'url-loader'
        }
    ]
  },
  /*modules: [
    'node_modules'
  ],*/
  /*resolve: {
    modules: ['node_modules'],
    alias: {
      //jquery: 'jquery',
      //angular: 'angular'
      //uiRouter: '@uirouter/angularjs'

    },     
  },*/
  
  plugins: [
    
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery'
    }),
    
    /*new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false }
    })*/
  ]
};
