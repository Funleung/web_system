module.exports = main => {
    main.component('app', {
        bindings: {appDetails: '<'},
        templateUrl: 'main/app.html',
        controller: function(AppService, $state) {
            const LIST_AMOUNT = 5;

            /* device lookup */
            this.mac;
            this.deviceExsits = true;

            /* build */
            this.build = "debug";

            /* issue list */
            this.filter;
            this.currentPage = 1;
            this.totalPage = 1;
            this.issueList;
            this.osVersionFilter;
            this.appVersionFilter;
            this.keywordOption = 'Stack Trace';
            this.keywordFilter;
            this.orderBy = "Latest Issue";

            this.deviceLookup = () => {
                $('#deviceId').modal('toggle');
                $state.go('deviceLookup', {appId: this.appDetails._id, mac: this.mac});
            }

            /*//load chart
            const data1 = [
                {
                    date: 'May 11',
                    crash: 0,
                    issue: 0
                },
                {
                    date: 'May 12',
                    crash: 0,
                    issue: 1
                },
                {
                    date: 'May 13',
                    crash: 0,
                    issue: 2
                },
                {
                    date: 'May 14',
                    crash: 1,
                    issue: 2
                },
                {
                    date: 'May 15',
                    crash: 2,
                    issue: 2
                }
            ];

            const data2 = [
                {
                    version: '5.1.0',
                    amount: 5 
                },
                {
                    version: '5.1.1',
                    amount: 3
                }
            ];

            const data3 = [
                {
                    version: '1.0.0',
                    amount: 13
                },
                {
                    version: '1.0.1',
                    amount: 9
                }
            ];

            let chart1;
            let chart2;
            let chart3;

            initChart = () => {
                chart1 = Highcharts.chart('Daily', {
                    chart: {
                        type: 'line',
                        height: 359
                    },
                    title: {
                        text: 'Recent crash(s)'
                    },
                    yAxis: {
                        allowDecimals: false,
                        title: {
                            text: 'Time(s)'
                        }
                    },
                    series: [
                        {
                            name: 'New Crash'
                        },
                        {
                            name: 'New Issue'
                        }
                    ]
                });

                chart2 = Highcharts.chart('OS', {
                    chart: {
                        type: 'bar',
                        height: 194
                    },
                    title: {
                        margin: -15,
                        text: 'OS Version',
                        align: 'right',
                        y: 150,
                        style: {
                            color: '#888s',
                            fontSize: '15px'
                        }       
                    },
                    yAxis: {
                        title: {
                            text: null
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    series: [
                        {
                            name: 'New Crash'
                        }
                    ]
                });

                chart3 = Highcharts.chart('App', {
                    chart: {
                        type: 'bar',
                        height: 194
                    },
                    title: {
                        margin: -15,
                        text: 'App Version',
                        align: 'right',
                        y: 150,
                        style: {
                            color: '#888s',
                            fontSize: '15px'
                        }       
                    },
                    yAxis: {
                        title: {
                            text: null
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    series: [
                        {
                            name: 'New Crash',
                        }
                    ]
                });
            };

            updateChart = () => {
                chart1.xAxis[0].setCategories(data1.map(obj => obj.date), false);
                chart1.series[0].setData(data1.map(obj => obj.crash), false);
                chart1.series[1].setData(data1.map(obj => obj.issue), true);
                
                chart2.xAxis[0].setCategories(data2.map(obj => obj.version), false);
                chart2.series[0].setData(data2.map(obj => obj.amount), true);

                chart3.xAxis[0].setCategories(data3.map(obj => obj.version), false);
                chart3.series[0].setData(data3.map(obj => obj.amount), true);
            };*/

            let upateResult = result => {
                if(result && result.count > 0){
                    this.totalPage = Math.ceil(result.count / LIST_AMOUNT);
                } else { //if no result
                    this.totalPage = 1;
                }
                this.issueList = result.issues;
            }

            const getIssues = filter => {
                AppService.getIssues(
                    this.appDetails._id,
                    {
                        build:   this.build,
                        orderBy: this.orderBy,
                        os:      this.osVersionFilter,
                        app:     this.appVersionFilter,
                        keyword: this.keywordFilter,
                        opt:     this.keywordOption,
                        page:    this.currentPage
                    }
                ).then(res => {
                    upateResult(res.data);
                });
            };

            //call by ui
            this.filterResult = () => {
                this.currentPage = 1;
                getIssues();
            };

            this.changeBuild = build => {
                this.build = build;
                this.reset();
            };

            this.previousPage = () => {
                if(this.currentPage > 1){
                    this.currentPage--;
                    getIssues();
                }
            };

            this.nextPage = () => {
                if(this.totalPage > this.currentPage){
                    this.currentPage++;
                    getIssues();
                }
            };

            this.changeKeyworkOption = option => {
                this.keywordOption = option;
            }

            this.changeOrder = option => {
                this.orderBy = option;
                this.currentPage = 1;
                getIssues();
            }

            this.reset = () => {
                this.filter = '';
                this.currentPage = 1;
                this.osVersionFilter = '';
                this.appVersionFilter = '';
                this.keywordOption = 'Stack Trace';
                this.keywordFilter = '';
                this.orderBy = "User Affected";
                getIssues();
            };

            this.$onInit = () => {
                /*initChart();
                updateChart();*/
                getIssues();
            };
        }
    });
}