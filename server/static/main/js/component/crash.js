module.exports = main => {
    main.component('crash', {
        bindings: {crashDetails: '<'},
        templateUrl: 'main/crash.html',
        controller: function(CrashService, $state){
            this.logTab = 1;
            this.envTab = 1;
            this.uptime;

            /*this.goBackApp = () => {
                $state.go('app', {appId: this.crashDetails.appId});
            }*/

            this.goBackIssue = () => {
                $state.go('issue', {issueId: this.crashDetails.ISSUE_ID});
            }

            this.changeLogTab = (tab) => {
                this.logTab = tab;
            };

            this.changeEnvTab = (tab) => {
                this.envTab = tab;
            };

            this.$onInit = () => {
                const seconds = this.crashDetails.CUSTOM_DATA.uptime;
                const day = Math.floor(seconds / 86400);
                const hour = Math.floor((seconds % 86400) / 3600);
                const minute = Math.floor((seconds % 3600) / 60);
                const second = seconds % 60;
                this.uptime = `${(day > 0 ? `${day} day(s), ` : '')}${hour}:${minute}:${second}`;

                this.envi = this.crashDetails.ENVIRONMENT;
                this.bui = this.crashDetails.BUILD;
                this.share = this.crashDetails.SHARED_PREFERENCES.default;
                this.cust = this.crashDetails.CUSTOM_DATA;
                this.ver = this.crashDetails.BUILD.VERSION;
                var crashlog = this.crashDetails.LOGCAT;
                var crashes = crashlog.split("\n");
                this.formatCrash = [];

                for(let i = 0; i < crashes.length; i++){
                    let tmp = crashes[i].match((/(\d\d-\d\d \d\d:\d\d:\d\d.\d\d\d) (\w)\/(\w+) *\( {0,4}(\d{1,5})\): (.*)/));
                    if(tmp){
                        this.formatCrash.push({
                            date:      tmp[1], 
                            filter:    tmp[2],
                            source:    tmp[3],
                            processid: tmp[4],
                            message:   tmp[5]
                        });
                    }
                }

                this.raw = this.crashDetails.STACK_TRACE.split("\n");

                this.cra = this.crashDetails.CRASH_CONFIGURATION;
                this.ini = this.crashDetails.INITIAL_CONFIGURATION;
                this.oth = {
                    'USER_EMAIL': this.crashDetails.USER_EMAIL,
                    'DEVICE_ID': this.crashDetails.DEVICE_ID,
                    'REPORT_ID': this.crashDetails.REPORT_ID,
                    'INSTALLATION_ID': this.crashDetails.INSTALLATION_ID,
                    'BRAND': this.crashDetails.BRAND,
                    'PRODUCT': this.crashDetails.PRODUCT,
                    'PHONE_MODEL': this.crashDetails.PHONE_MODEL,
                    'AVAILABLE_MEM_SIZE': this.crashDetails.AVAILABLE_MEM_SIZE,
                    'TOTAL_MEM_SIZE': this.crashDetails.TOTAL_MEM_SIZE,
                    'USER_APP_START_DATE': this.crashDetails.USER_APP_START_DATE,
                    'USER_CRASH_DATE': this.crashDetails.USER_CRASH_DATE,
                    'FILE_PATH': this.crashDetails.FILE_PATH,
                    'IS_SILENT': this.crashDetails.IS_SILENT,
                    'DEVICE_FEATURES': this.crashDetails.DEVICE_FEATURES
                };
                this.dis = this.crashDetails.DISPLAY;
                this.isObject = angular.isObject;
            }
            
        }
    });
}