module.exports = main => {
    main.component('deviceLookup', {
        bindings: {appDetails: '<'},
        templateUrl: 'main/deviceLookup.html',
        controller: function(AppService, $state) {
            const LIST_AMOUNT = 5;

            //current mac
            this.device;

            /* device lookup */
            this.macField;
            //this.deviceExsits = true;

            /* build */
            this.build = "debug";

            /* issue list */
            this.filter;
            this.currentPage = 1;
            this.totalPage = 1;
            this.issueList;
            this.osVersionFilter;
            this.appVersionFilter;
            this.keywordOption = 'Stack Trace';
            this.keywordFilter;
            this.orderBy = "Latest Issue";

            this.deviceLookup = () => {
                $('#deviceId').modal('toggle');
                $state.go('deviceLookup', {appId: this.appDetails._id, mac: this.macField});
            }

            this.changeBuild = build => {
                this.build = build;
                this.reset();
            };

            this.changeDevice = index => {
                this.device = this.appDetails.macs[index];
                this.reset();
            }

            const upateResult = result => {
                if(result && result.count > 0){
                    this.totalPage = Math.ceil(result.count / LIST_AMOUNT);
                } else { //if no result
                    this.totalPage = 1;
                }
                this.issueList = result.issues;
            }

            const getIssues = () => 
                AppService.getIssues(
                    this.appDetails._id,
                    {
                        mac:     this.device,
                        build:   this.build,
                        orderBy: this.orderBy,
                        os:      this.osVersionFilter,
                        app:     this.appVersionFilter,
                        keyword: this.keywordFilter,
                        opt:     this.keywordOption,
                        page:    this.currentPage
                    }
                ).then(res => upateResult(res.data));

            this.filterResult = () => {
                this.currentPage = 1;
                getIssues();
            };

            this.previousPage = () => {
                if(this.currentPage > 1){
                    this.currentPage--;
                    getIssues();
                }
            };

            this.nextPage = () => {
                if(this.totalPage > this.currentPage){
                    this.currentPage++;
                    getIssues();
                }
            };

            this.changeKeyworkOption = option => {
                this.keywordOption = option;
            }

            this.changeOrder = option => {
                this.orderBy = option;
                this.currentPage = 1;
                getIssues();
            }

            this.reset = () => {
                this.filter = '';
                this.currentPage = 1;
                this.osVersionFilter = '';
                this.appVersionFilter = '';
                this.keywordOption = 'Stack Trace';
                this.keywordFilter = '';
                this.orderBy = "Latest Issue";
                getIssues();
            };

            this.$onInit = () => {
                this.device = this.appDetails.macs[0];
                getIssues();
            };
        }
    });
}