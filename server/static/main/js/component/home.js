const md5 = require('blueimp-md5');

module.exports = main => {
    main.component('home', {
        bindings: {apps: '<', me: '<'},
        templateUrl: `main/home.html`,
        controller: function(AppService, $http){
            this.protocol = location.protocol;
            this.hostname = location.hostname;

            this.createdNewApp = false;
            this.newPlatform = "Android";
            this.newAppName;
            this.newAPIKey;

            let updateApps = () => {
                this.androidApps = this.apps.filter(app => app.platform === 'Android');
                this.linuxApps = this.apps.filter(app => app.platform === 'Linux');
                this.windowsApps = this.apps.filter(app => app.platform === 'Windows');
            }

            this.$onInit = () => updateApps();

            this.createNewApp = () => {
                this.newAPIKey = md5(Date.now());
                
                $http.post('api/apps', {
                    API_KEY: this.newAPIKey,
                    PLATFORM: this.newPlatform,
                    APP_NAME: this.newAppName
                }).then(res => {
                    this.createdNewApp = true;
                    AppService.getAllApps().then(res => {
                        this.apps = res.data;
                        updateApps();
                    });
                });
            }
        }
    });
}