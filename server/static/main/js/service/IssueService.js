module.exports = main => {
    main.service('IssueService', function($http){
        return {
            getIssue: issueId => $http.get(`api/issues/${issueId}`),
            getCrashes: (issueId, filter = {}) => {
                let params = { issueId: issueId };
                    
                if (filter.orderBy) {
                    params.orderBy = filter.orderBy;
                }
                if (filter.os) {
                    params.os = filter.os;
                }
                if (filter.mac) {
                    params.mac = filter.mac;
                }
                if (filter.keyword) {
                    params.keyword = filter.keyword;
                    params.opt = filter.opt;
                }
                if (filter.page) {
                    params.page = filter.page;
                }

                return $http.get('api/crashes', { params: params });
            }
        }
    });
}