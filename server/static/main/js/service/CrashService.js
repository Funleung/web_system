module.exports = main => {
    main.service('CrashService', function($http){
        return {
            getCrashDetails: (crashId) => { 
                return $http.get(`api/crashes/${crashId}`);
            }
        }
    });
}