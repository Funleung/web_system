module.exports = main => {
    main.service('UserService', function($http){
        return {
            getMe: () => $http.get('api/user/me', {cache: true})
        };
    });
}