module.exports = main => {
    main.service('AppService', function($http){
        return {
            getAllApps: () => $http.get('api/apps'),
            getAppDetails: appId => $http.get(`api/apps/${appId}`),
            getDeviceLookupDetails: (appId, mac) => $http.get(`api/apps/${appId}`, {params: {mac: mac}}),
            getIssues: (appId, filter = {}) => {
                let params = { appId: appId };

                if (filter.build) {
                    params.build = filter.build;
                }
                if (filter.orderBy) {
                    params.orderBy = filter.orderBy;
                }
                if (filter.mac) {
                    params.mac = filter.mac;
                }
                if (filter.os) {
                    params.os = filter.os;
                }
                if (filter.app) {
                    params.app = filter.app;
                }
                if (filter.keyword) {
                    params.keyword = filter.keyword;
                    params.opt = filter.opt;
                }
                if (filter.page) {
                    params.page = filter.page;
                }

                return $http.get('api/issues', { params: params });
            }
        }
    });
}