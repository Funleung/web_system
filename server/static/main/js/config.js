module.exports = main => {
    main.config(function($stateProvider){
        const states = [
            {
                name: 'home',
                url: '',
                component: 'home',
                resolve: {
                    me: UserService => UserService.getMe().then(res => res.data),
                    apps: AppService => AppService.getAllApps().then(res => res.data)
                }
            },
            {
                name: 'app',
                url: '/app/{appId}',
                component: 'app',
                resolve: {
                    appDetails: (AppService, $stateParams) => 
                        AppService.getAppDetails($stateParams.appId).then(res => res.data)
                }
            },
            {
                name: 'deviceLookup',
                url: '/app/{appId}?mac',
                component: 'deviceLookup',
                resolve: {
                    appDetails: (AppService, $stateParams) => 
                        AppService.getDeviceLookupDetails($stateParams.appId, $stateParams.mac).then(res => res.data)
                }
            },
            {
                name: 'issue',
                url: '/issue/{issueId}',
                component: 'issue',
                resolve: {
                    issueDetails: (IssueService, $stateParams) => 
                        IssueService.getIssue($stateParams.issueId).then(res => res.data)
                }
            },
            {
                name: 'crash',
                url: '/crash/{crashId}',
                component: 'crash',
                resolve: {
                    crashDetails: (CrashService, $stateParams) => 
                        CrashService.getCrashDetails($stateParams.crashId).then(res => res.data)
                }
            },
            {
                name: 'setting',
                url: '/setting/',
                component: 'setting'
            }
        ];

        states.forEach(state => $stateProvider.state(state));
    });
}