import '../css/bootstrap.min.css';
import '../css/appDetails.css';
import '../css/errorLog.css';
import '../css/index.css';

import 'angular';
import '@uirouter/angularjs';
import 'jquery';
import 'bootstrap';
//import Highcharts from 'highcharts';

const main = angular.module("Main", ['ui.router']);

//import state config
require('./config')(main);

//import services
require('./service/AppService')(main);
require('./service/CrashService')(main);
require('./service/IssueService')(main);
require('./service/UserService')(main);

//import components
require('./component/home')(main);
require('./component/app')(main);
require('./component/deviceLookup')(main);
require('./component/issue')(main);
require('./component/crash')(main);
require('./component/setting')(main);
