import '../css/bootstrap.css';
import '../css/login.css';

import 'jquery';
import 'angular';
import 'bootstrap';
import '@uirouter/angularjs';

const app = angular.module('login', ['ui.router']);

app.controller('LoginController', ['$http', function($http){
  this.valid = true;
  this.message = "";

  this.login = () => {
    $http.post('/login', {
      username: this.username,
      password: this.password
    })
    .then(
      res => { //request success
        if(res.data.status === 'success'){ //success
          location.reload(true);
        } else {                           //error
          this.valid = false;
          this.message = res.data.message;
        }
      }, res => { //request failed
        this.valid = false;
        this.message = "Connection problem";
      }
    );
  }
}]);