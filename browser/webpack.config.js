const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: './src/main.js',
  output: {
    filename: '../server/static/js/bundle.js'
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },
  module: {
         loaders: [
             {
                 test: /\.js$/,
                 loader: 'babel-loader',
                 query: {
                     presets: ['es2015']
                 }
             },
             {
                test: /\.vue$/,
                loader: 'vue-loader'
             },
             {
               test: /\.css$/,
               use: ExtractTextPlugin.extract({
                 fallback: "style-loader",
                 use: "css-loader"
               })
             }
         ]
  },
  plugins: [
   new ExtractTextPlugin("../server/static/css/styles.css"),
  ],
  stats: {
    colors: true
  },
  devtool: 'source-map'
};
