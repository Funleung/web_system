require("webrtc-adapter")
var io = require("socket.io-client")

const SERVERS = {
  'iceServers': [{
    'url': 'stun:stun.l.google.com:19302'
  }]
};

const offerOptions = {
   offerToReceiveAudio: 0,
   offerToReceiveVideo: 1
};

class webrtc_client{

  constructor(){
    this.pc             = null;
    this.sendChannel    = null;
    this.socket         = null;
    this.channelOpened  = false;

    this.onCreateOfferSuccess = this.onCreateOfferSuccess.bind(this);
    this.onSetLocalSuccess    = this.onSetLocalSuccess.bind(this);
  }

  enter_room(url, room, callback){

    console.log("Connecting to room: " + url + ", in room: " + room);

    this.socket = io(url, {
      extraHeaders: {
        Authorization: "Bearer authorization_token_here"
      }
    });

    this.socket.on('connect', ()=>{
      console.log('Connected: ' + room);
      this.socket.emit('authentication', {username: "John", password: "secret"});
      this.socket.emit('room', room);
    })

    this.socket.on('status', (status)=>{
      if(status.room){
        console.log("Entered room: " + status.room);
        this.connect(callback);
      }

    })

    this.socket.on('error', (msg)=>{
      console.log(msg);
    })

    this.socket.on('disconnect', ()=>{
      console.log('Disconnected: ' + room);
    })

    this.socket.on('unauthorized', (err)=>{
      console.log('There was an error with the authentication: ' + err.message);
    })

  }

  connect(callback){
    this.pc             = new RTCPeerConnection(SERVERS);
    this.sendChannel    = this.pc.createDataChannel('sendDataChannel', null);

    this.sendChannel.onopen = function(){
      var readyState = sendChannel.readyState;
      console.log("ReadyState: " + readyState);
      channelOpened=true;
    };


    this.pc.onaddstream = (event)=> {
      var url = URL.createObjectURL(event.stream);
      console.log("Url: " + url);
      callback(url);
    };

    this.pc.createOffer(
      offerOptions
    ).then(this.onCreateOfferSuccess,(error)=>{
        console.log(error)
      }
    );

    this.pc.onicecandidate = (desc)=> {
      console.log(desc.candidate);
      var sdp = JSON.stringify({ "caller_candidates": desc.candidate });
      this.socket.emit('calling', sdp);
    };

    this.socket.on('answer', (msg)=>{
      //console.log(msg);
      var signal = JSON.parse(msg);
      //console.log(signal.caller);

      if(signal.callee){

          this.pc.setRemoteDescription(new RTCSessionDescription(signal.callee)).then(
            ()=>{
              console.log("Setting Remote Description");
            },
            (error)=>{
              console.log(error)
            }
          )

      }
    }
    );

  }

  onCreateOfferSuccess(desc){
    //console.log("Offer succeded: " + desc);
    console.log("Offer succeded");
    this.pc.setLocalDescription(desc).then(
      ()=> {
        this.onSetLocalSuccess(desc);
      },
      function(error){console.log(error)}
    );
 }

 onSetLocalSuccess(desc) {
   //console.log(desc);
   var sdp = JSON.stringify({ "caller": desc });
   //console.log(sdp);
   this.socket.emit('calling', sdp);
 }

 toString(){
   return 'Hello visitor: ' + this.pc;
 }

}
export default webrtc_client
