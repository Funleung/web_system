const queryString = require('query-string')
const URL         = require('url-parse')

import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.use(VueRouter);

import main from './app.vue'

/*
new Vue({
  el: 'app',
  components: { App }
})*/

const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

/*
var router = new VueRouter();

router.map({
  '/': {
    component: main
  }
});
*/
var App = Vue.extend({});

//router.start(App, '#app');

/*
import webrtc_client from './webrtc_client.js'

console.log('webpack is working!');

var client = new webrtc_client();

window.call = function call(){
  var url     = new URL(window.location.href);
  var parsed  = queryString.parse(window.location.search);
  var room    = parsed.room
  var baseUrl = url.protocol + "//" + url.hostname + ":" + url.port;
  console.log(baseUrl);
  console.log(client.enter_room(baseUrl,room));
}*/
